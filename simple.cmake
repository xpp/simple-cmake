#used modules (all external modules are added here to provide an overview)
include(CMakeParseArguments)
include(CheckCXXCompilerFlag)
include(CMakePackageConfigHelpers)

#packages
#find_package(Doxygen QUIET COMPONENTS dot doxygen)
find_package(Doxygen)
find_package(Boost QUIET COMPONENTS unit_test_framework)

#options / config / variables
include(${CMAKE_CURRENT_LIST_DIR}/simple-cmake/options.cmake           )

#internal functions etc
include(${CMAKE_CURRENT_LIST_DIR}/simple-cmake/internal.cmake          )

#print
_scm_status("Module 'simple-cmake' from ${scm_root_file}")

#functions
include(${CMAKE_CURRENT_LIST_DIR}/simple-cmake/utility.cmake           )

#targets
include(${CMAKE_CURRENT_LIST_DIR}/simple-cmake/doxygen.cmake           )
include(${CMAKE_CURRENT_LIST_DIR}/simple-cmake/dependency_graph.cmake  )
include(${CMAKE_CURRENT_LIST_DIR}/simple-cmake/generate.cmake          )
include(${CMAKE_CURRENT_LIST_DIR}/simple-cmake/coverage.cmake          )
include(${CMAKE_CURRENT_LIST_DIR}/simple-cmake/add_target.cmake        )

#finish project
include(${CMAKE_CURRENT_LIST_DIR}/simple-cmake/install_and_export.cmake)
