# simple-cmake

(Examples and a more verbose documentation are in progress.)

This library provides convenience features for cmake, such as:
* automatically generating config and version scripts
* taking care of installation
* providing easy to use commands such as add\_header\_library
* selecting a compiler
* functions for cmake debugging


## Documentation

You can build the documentation by executing

    git clone https://gitlab.com/xpp/simple-cmake
    mkdir -p simple-cmake/build
    cd simple-cmake/build
    cmake ..
    make doc

or by visiting https://xpp.gitlab.io/simple-cmake.

## How to use simple-cmake

Currently there are two ways of using `simple-cmake` in your project. Via find package or by calling `include(...) in cmake.

### Via `find_package(...)`

FIrst you have to make `simple-cmake` available to cmake.
To do this, just clone and build the repo.
Then it can be used from the build tree via `find_package(simple-cmake)`.

    git clone https://gitlab.com/xpp/simple-cmake
    mkdir -p simple-cmake/build
    cd simple-cmake/build
    cmake ..

In your project's cmake

    find_package(simple-cmake)

### Via `include(...)`

Clone `simple cmake` into yout project (e.g. add it as git submodule) and call include in cmake.

    include(path/to/simple-cmake/simple.cmake)


