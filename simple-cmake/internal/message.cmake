function(_scm_verbose)
        message(STATUS "${scm_msg_prefix}${ARGN}")
endfunction()

function(_scm_status)
    message(STATUS "${scm_msg_prefix}${ARGN}")
endfunction()

function(_scm_warning)
    message(WARNING "${scm_msg_prefix}WARNING: ${ARGN}")
endfunction()

function(_scm_fatal)
    message(FATAL_ERROR "${scm_msg_prefix}ERROR: ${ARGN}")
endfunction()

if("${scm_msg_level}" STREQUAL "VERBOSE")
    #noting to deactivate
elseif("${scm_msg_level}" STREQUAL "STATUS")

    function(_scm_verbose)
    endfunction()

elseif("${scm_msg_level}" STREQUAL "WARNING")

    function(_scm_verbose)
    endfunction()
    function(_scm_status)
    endfunction()
    function(_scm_warning)
    endfunction()

elseif("${scm_msg_level}" STREQUAL "ERROR")

    function(_scm_verbose)
    endfunction()
    function(_scm_status)
    endfunction()
    function(_scm_warning)
    endfunction()

else()
    message(FATAL_ERROR "${scm_msg_prefix}ERROR: Unknown value for scm_msg_level: ${scm_msg_level}")
endif()
