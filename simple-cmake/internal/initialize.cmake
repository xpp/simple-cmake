if(NOT "${scm_internal_variables}" STREQUAL "")
    foreach(var ${scm_internal_variables})
    set(${var} CACHE INTERNAL "cleared internal scm variable")
    endforeach()
    set(${scm_internal_variables} CACHE INTERNAL "cleared internal scm variable")
endif()

