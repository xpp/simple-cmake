function(_scm_set_internal var)
    list(APPEND scm_internal_variables ${var})
    list(REMOVE_DUPLICATES scm_internal_variables)
    set(scm_internal_variables ${scm_internal_variables} CACHE INTERNAL "all internal scm variables")

    set(${var} ${ARGN} CACHE INTERNAL "internal scm variable ${var}")
endfunction()

function(_scm_register_current_project)
    _scm_register_project(${CMAKE_PROJECT_NAME})
endfunction()

function(_scm_register_project proj)
    list(APPEND scm_project_names ${proj})
    list(REMOVE_DUPLICATES scm_project_names)
    _scm_set_internal(scm_project_names ${scm_project_names})
endfunction()

function(_scm_register_library_target targ)
    _scm_register_current_project()
    set(var scm_${CMAKE_PROJECT_NAME}_libraries)
    _scm_set_internal(${var} ${${var}} ${targ})
endfunction()

