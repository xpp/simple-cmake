function(__scm_internal_add_dep_graph_targets)
    find_program(dot_executable dot)
    if(dot_executable)
        set(scm_dependency_graph_prefix_exc ${scm_dependency_graph_dir}/deps.dot)
        set(scm_dependency_graph_prefix_inc ${scm_dependency_graph_dir}/deps.external.dot)

        #clean target
        add_custom_target(dependency_graph_exc_external_clean
            COMMAND ${CMAKE_COMMAND} -E make_directory  ${scm_dependency_graph_dir}
            COMMAND ${CMAKE_COMMAND} -E touch           ${scm_dependency_graph_prefix_exc}  #stops errors of next line
            COMMAND ${CMAKE_COMMAND} -E remove          ${scm_dependency_graph_prefix_exc}*
        )
        add_custom_target(dependency_graph_inc_external_clean
            COMMAND ${CMAKE_COMMAND} -E make_directory  ${scm_dependency_graph_dir}
            COMMAND ${CMAKE_COMMAND} -E touch           ${scm_dependency_graph_prefix_inc}  #stops errors of next line
            COMMAND ${CMAKE_COMMAND} -E remove          ${scm_dependency_graph_prefix_inc}*
        )

        #graphviz target
        set(graphviz_option_file ${CMAKE_BINARY_DIR}/CMakeGraphVizOptions.cmake)
        add_custom_target(dependency_graph_exc_external_dot
            COMMAND echo ' set ( GRAPHVIZ_EXTERNAL_LIBS FALSE ) ' > ${graphviz_option_file}
            COMMAND ${CMAKE_COMMAND} --graphviz=${scm_dependency_graph_prefix_exc} ${CMAKE_SOURCE_DIR} > /dev/null 2> /dev/null
            COMMENT "Generating dot files excluding external dependencies"
        )
        add_dependencies(dependency_graph_exc_external_dot dependency_graph_exc_external_clean)

        add_custom_target(dependency_graph_inc_external_dot
            COMMAND echo ' set ( GRAPHVIZ_EXTERNAL_LIBS TRUE ) ' > ${graphviz_option_file}
            COMMAND ${CMAKE_COMMAND} --graphviz=${scm_dependency_graph_prefix_inc} ${CMAKE_SOURCE_DIR} > /dev/null 2> /dev/null
            COMMENT "Generating dot files including external dependencies"
        )
        add_dependencies(dependency_graph_inc_external_dot dependency_graph_inc_external_clean)

        #dot target
        add_custom_target(dependency_graph_exc_external_svg
            COMMAND ls -1 ${scm_dependency_graph_prefix_exc}.* | grep -v .svg$ | xargs -I% bash -c 'dot -Tsvg -O % && rm %'
        )
        add_custom_target(dependency_graph_inc_external_svg
            COMMAND ls -1 ${scm_dependency_graph_prefix_inc}.* | grep -v .svg$ | xargs -I% bash -c 'dot -Tsvg -O % && rm %'
        )

        #add glue targets
        add_dependencies(dependency_graph_exc_external_svg dependency_graph_exc_external_dot)
        add_dependencies(dependency_graph_inc_external_svg dependency_graph_inc_external_dot)

        add_custom_target(dependency_graph_exc_external)
        add_dependencies( dependency_graph_exc_external dependency_graph_exc_external_svg)

        add_custom_target(dependency_graph_inc_external)
        add_dependencies ( dependency_graph_inc_external dependency_graph_inc_external_svg)

        add_custom_target(dependency_graph)
        add_dependencies( dependency_graph dependency_graph_inc_external dependency_graph_exc_external)
    else()
        _scm_status("Executable graphviz not found! Can't add target dependency_graph.")
    endif()
endfunction()
