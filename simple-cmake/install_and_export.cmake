#TODO
#check if vars set -> if not set them to default and message

function(scm_add_utility_targets)
    __scm_internal_coverage_targets()       #TODO make callable multiple times
    __scm_internal_add_doc_targets()        #TODO make callable multiple times
    __scm_internal_add_dep_graph_targets()  #TODO make callable multiple times
endfunction()

function(scm_write_config_and_version_files)

    _scm_status("CONFIGURING FILE '${scm_config_in_file}' -> '${scm_config_out_file}'")
    scm_add_utility_targets()
    configure_package_config_file(
        ${scm_config_in_file}
        ${scm_config_out_file}
        INSTALL_DESTINATION ${scm_install_dir_cmake}
    )
    _scm_status("WRITING VERSION FILE '${scm_version_out_file}', VERSION '${PROJECT_VERSION}'")
    write_basic_package_version_file(
        ${scm_version_out_file}
        VERSION ${PROJECT_VERSION}
        COMPATIBILITY AnyNewerVersion
    )
endfunction()

function(scm_export_project)
    foreach(exp_name ${scm_export_names})
        export(EXPORT ${exp_name} NAMESPACE ${scm_export_namespace} FILE ${exp_name}-targets.cmake)
    endforeach()

    export(PACKAGE ${CMAKE_PROJECT_NAME})
endfunction()

function(scm_install_project_export)
    foreach(exp_name ${scm_export_names})
        install(EXPORT ${exp_name} DESTINATION ${scm_install_dir_cmake} NAMESPACE ${scm_export_namespace})
    endforeach()
endfunction()

function(scm_install_project_config_files)
    install(FILES
        ${scm_config_out_file}
        ${scm_version_out_file}
        DESTINATION ${scm_install_dir_cmake}
    )
endfunction()

function(scm_install_project)
    scm_install_project_export()
    scm_install_project_config_files()
endfunction()

function(scm_finalize_project)
    scm_write_config_and_version_files()
    scm_export_project()
    scm_install_project()
endfunction()
