function(__scm_internal_coverage_targets)
    if(scm_build_coverage AND NOT scm_build_tests)
        _scm_fatal("Activated code coverage but deactivated testing")
    endif()

    if(scm_build_coverage)
        if(NOT lcov_flags)
            set(lcov_flags)
            set(lcov_flags "${lcov_flags} --capture")
            set(lcov_flags "${lcov_flags} --no-external")
            set(lcov_flags "${lcov_flags} --gcov-tool ${scm_gcov_tool}")
            set(lcov_flags "${lcov_flags} --directory ${CMAKE_SOURCE_DIR}")
        endif()
        set(lcov_flags "${lcov_flags} --output-file ${scm_coverage_file}")

        if(NOT genhtml_flags)
            set(genhtml_flags)
            set(genhtml_flags "${genhtml_flags} --show-details")
            set(genhtml_flags "${genhtml_flags} --frames")
            set(genhtml_flags "${genhtml_flags} --keep-descriptions")
            set(genhtml_flags "${genhtml_flags} --num-spaces 4")
            set(genhtml_flags "${genhtml_flags} --sort")
            set(genhtml_flags "${genhtml_flags} --function-coverage")
            set(genhtml_flags "${genhtml_flags} --branch-coverage")
            set(genhtml_flags "${genhtml_flags} --demangle-cpp")
        endif()
        set(genhtml_flags "${genhtml_flags} --output-directory ${scm_coverage_dir}/html")
        set(genhtml_flags "${genhtml_flags} ${scm_coverage_file}")

        file(WRITE  ${CMAKE_BINARY_DIR}/run_tests "#!/bin/bash\n")
        file(APPEND ${CMAKE_BINARY_DIR}/run_tests "echo -e '\\033[1;34m#################CTest#################\\033[0m'\n")
        file(APPEND ${CMAKE_BINARY_DIR}/run_tests "mkdir -p ${scm_coverage_dir}/html\n")
        file(APPEND ${CMAKE_BINARY_DIR}/run_tests "ctest || true\n")
        add_custom_target(run_tests
            COMMAND ${CMAKE_BINARY_DIR}/run_tests
            WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
        )

        file(WRITE  ${CMAKE_BINARY_DIR}/run_lcov "#!/bin/bash\n")
        file(APPEND ${CMAKE_BINARY_DIR}/run_lcov "echo -e '\\033[1;34m#################lcov##################\\033[0m'\n")
        file(APPEND ${CMAKE_BINARY_DIR}/run_lcov "mkdir -p ${scm_coverage_dir}/html\n")
        file(APPEND ${CMAKE_BINARY_DIR}/run_lcov "echo -e '\\033[1;34mTool : ${scm_lcov_tool}\\033[0m'\n")
        file(APPEND ${CMAKE_BINARY_DIR}/run_lcov "echo -e '\\033[1;34mFlags:\n${lcov_flags}    \\033[0m'\n")
        file(APPEND ${CMAKE_BINARY_DIR}/run_lcov "${scm_lcov_tool} ${lcov_flags} \n")
        add_custom_target(run_lcov
            COMMAND ${CMAKE_BINARY_DIR}/run_lcov
            WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
        )

        file(WRITE  ${CMAKE_BINARY_DIR}/run_genhtml "#!/bin/bash\n")
        file(APPEND ${CMAKE_BINARY_DIR}/run_genhtml "echo -e '\\033[1;34m################genhtml################\\033[0m'\n")
        file(APPEND ${CMAKE_BINARY_DIR}/run_genhtml "mkdir -p ${scm_coverage_dir}/html\n")
        file(APPEND ${CMAKE_BINARY_DIR}/run_genhtml "echo -e '\\033[1;34mTool : ${scm_genhtml_tool}\\033[0m'\n")
        file(APPEND ${CMAKE_BINARY_DIR}/run_genhtml "echo -e '\\033[1;34mFlags:\n${genhtml_flags}\\033[0m'\n")
        file(APPEND ${CMAKE_BINARY_DIR}/run_genhtml "${scm_genhtml_tool} ${genhtml_flags}  \n")
        add_custom_target(run_genhtml
            COMMAND ${CMAKE_BINARY_DIR}/run_genhtml
            WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
        )

        add_custom_target(coverage
            COMMAND make run_tests
            COMMAND make run_lcov
            COMMAND make run_genhtml
            WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
        )
    endif()
endfunction()

function(scm_add_coverage target)
    if(scm_build_coverage)
        target_compile_options(${target} PUBLIC -ftest-coverage)
        target_compile_options(${target} PUBLIC -fprofile-arcs)
        target_link_libraries (${target} PUBLIC --coverage)
        target_link_libraries (${target} PUBLIC gcov)
    endif()
endfunction()
