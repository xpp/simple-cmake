#root dir / file
get_filename_component(scm_root_dir "${CMAKE_CURRENT_LIST_FILE}" DIRECTORY)
get_filename_component(scm_root_dir "${scm_root_dir}/.." ABSOLUTE)

set(scm_root_dir     "${scm_root_dir}"              CACHE INTERNAL "")
set(scm_root_file    "${scm_root_dir}/simple.cmake" CACHE INTERNAL "")
set(scm_cfg_file_dir "${scm_root_dir}/config-files" CACHE INTERNAL "")

function(_scm_do_option_parse)
    #reset vars
    set(types)
    set(names)
    set(descrs)

    set(cbd "${CMAKE_BINARY_DIR}"  )
    set(cpn "${CMAKE_PROJECT_NAME}")
    set(cpv "${PROJECT_VERSION}"   )

    #############################################
    #helper
    macro(opt type name val descr)
        set(${name} "${val}")
        list(APPEND types  "${type}")
        list(APPEND names  "${name}")
        list(APPEND descrs "${descr}")
    endmacro()

    macro(set_if_undef var)
        if(NOT DEFINED ${var})
            set(${var} ${ARGN})
        endif()
    endmacro()

    #############################################
    #options
    opt(OPT build_tests                ON                                    "Build tests")
    opt(OPT build_examples             ON                                    "Build examples")
    opt(OPT build_coverage             ON                                    "Build coverage")

    #tools
    opt(PAT gcov_tool                  gcov                                  "Path to gcov")
    opt(PAT lcov_tool                  lcov                                  "Path to lcov")
    opt(PAT genhtml_tool               genhtml                               "Path to genhtml")

    #messaging
    opt(STR msg_level                  "VERBOSE"                             "VERBOSE, STATUS, WARNING, ERROR")
    opt(STR msg_prefix                 "[SCM] "                              "")

    #dirs / files
    opt(STR dependency_graph_dir       "${cbd}/dependency_graph"             "")

    opt(STR coverage_dir               "${cbd}/coverage"                     "")
    opt(STR coverage_file              "${coverage_dir}/coverage.info"       "")

    opt(STR version_out_file_name      "${cpn}-config-version.cmake"         "")
    opt(STR version_out_file           "${cbd}/${version_out_file_name}"     "")

    opt(STR config_out_file_name       "${cpn}-config.cmake"                 "")
    opt(STR config_out_file            "${cbd}/${config_out_file_name}"      "")

    opt(STR config_in_file             "${scm_cfg_file_dir}/config.cmake.in" "")

    #file extend
    opt(STR source_extend              "c;cpp;cxx"                           "")
    opt(STR header_extend              "h;hpp;ipp;tpp"                       "")

    #export names
    opt(STR default_export             "${cpn}"                              "")
    opt(STR export_namespace           "${cpn}::"                            "")

    #install
    opt(STR install_dir_header         "include/${cpn}-${cpv}"               "")
    opt(STR install_dir_header_private "${install_dir_header}/_private"      "")
    opt(STR install_dir_library        "lib"                                 "")
    opt(STR install_dir_archive        "lib"                                 "")
    opt(STR install_dir_runtime        "bin"                                 "")
    opt(STR install_dir_objects        "share/${cpn}-${cpv}/objects"         "")
    opt(STR install_dir_resource       "share/${cpn}-${cpv}/resources"       "")
    opt(STR install_dir_cmake          "share/${cpn}-${cpv}/cmake"           "")
    opt(STR install_dir_doc            "share/${cpn}-${cpv}/doc"             "")

    #############################################
    #
    list(LENGTH types  ortypeslen )
    list(LENGTH names  ornameslen )
    list(LENGTH descrs ordescrslen)
    if(
        NOT ${ornameslen} EQUAL ${ortypeslen} OR
        NOT ${ornameslen} EQUAL ${ordescrslen}
    )
        message(FATAL_ERROR
            "[SCM] INTERNAL ERROR! lists have different lengths."
            "\nlen(names ) = ${ornameslen}"
            "\nlen(types ) = ${ortypeslen}"
            "\nlen(descrs) = ${ordescrslen}"
            )
    endif()

    #parse
    cmake_parse_arguments(
        _scm_config "" "" "${names}"
        ${simple-cmake_FIND_COMPONENTS})
    #set if not set
    foreach(n ${names})
        set_if_undef(_scm_config_${n} "${${n}}")
    endforeach()
    #add opts
    set(idx 0)
    while(${idx} LESS ${ornameslen})
        list(GET names  ${idx} name )
        list(GET types  ${idx} type )
        list(GET descrs ${idx} descr)

        set(val "${_scm_config_${name}}")
        set(scmname "scm_${name}")

        if("${type}" STREQUAL OPT)
            option("${scmname}" "${descr}" ${val})
        elseif("${type}" STREQUAL STR)
            set(   "${scmname}" "${val}" CACHE STRING   "${descr}")
        elseif("${type}" STREQUAL PAT)
            set(   "${scmname}" "${val}" CACHE FILEPATH "${descr}")
        else()
            message(FATAL_ERROR
                "[SCM] INTERNAL ERROR!"
                "\n idx   = ${idx}"
                "\n name  = ${type}"
                "\n type  = ${type}"
                "\n descr = ${descr}"
            )
        endif()
        math(EXPR idx "${idx} + 1")
    endwhile()
endfunction()
_scm_do_option_parse()
