macro(scm_generate_subdir_headers dir prefix_to_remove out_headers)
    string(REPLACE "//" "/" prefix_to_remove_fixed "${prefix_to_remove}/")
    string(REPLACE "${prefix_to_remove_fixed}" "" dir_rel "${dir}")
    _scm_status("     Generate subdir headers for: '${dir_rel}' ('${dir}')")
    _scm_generate_subdir_headers_impl("${dir}" "${prefix_to_remove_fixed}" ${out_headers})
endmacro()

macro(_scm_generate_subdir_headers_impl dir prefix_to_remove out_headers)
    scm_subdirs(subdirs ${dir})
    foreach(subdir ${subdirs})
        #recurse
        _scm_generate_subdir_headers_impl("${subdir}" "${prefix_to_remove}" ${out_headers})

        file(GLOB headers ${subdir}/*.hpp) #TODO use header extend variable
        list(LENGTH headers n)
        string(REGEX REPLACE ".*/" "" subdir_name "${subdir}")
        if(NOT ${n} EQUAL 0 AND NOT "${subdir_name}" STREQUAL "detail")
            set(subdir_header_abs "${subdir}.hpp")
            #string(REPLACE "${prefix_to_remove}" "" subdir_header "${subdir_header_abs}")
            list(APPEND ${out_headers} ${subdir_header_abs})
            #create file content
            set(content "#pragma once\n\n// This file is generated!\n\n")
            foreach(header ${headers})
                string(REPLACE "${subdir}/" "${subdir_name}/" h "${header}")
                set(content "${content}#include \"${h}\"\n")
            endforeach()
            #check for file change
            scm_update_file("${subdir_header_abs}" "${content}")
            if(${scm_file_up_to_date} EQUAL 0)
                _scm_status("         File updated: '${subdir_header}'")
            else()
                _scm_status("         File already up to date: '${subdir_header}'")
            endif()
        endif()
    endforeach()
endmacro()

function(scm_generate_project_meta_target)
    #define params
    set(flags)
    set(single_param SOURCE_DIRECTORY_PREFIX)
    set(multi_param)
    cmake_parse_arguments(PARAM "${flags}" "${single_param}" "${multi_param}" ${ARGN} )
    ################################################################################
    #check params
    if(NOT "${PARAM_UNPARSED_ARGUMENTS}" STREQUAL "")
        _scm_fatal("some arguments were not parsed: ${PARAM_UNPARSED_ARGUMENTS}\nARGN: ${ARGN}")
    endif()
    set(_scm_source_dir ${CMAKE_CURRENT_LIST_DIR})
    if(NOT "${PARAM_SOURCE_DIRECTORY_PREFIX}" STREQUAL "")
        string(APPEND _scm_source_dir "/${PARAM_SOURCE_DIRECTORY_PREFIX}")
    endif()

    set(_scm_target_file ${CMAKE_PROJECT_NAME}/${CMAKE_PROJECT_NAME}.hpp)
    set(_scm_target_file_abs "${_scm_source_dir}/${_scm_target_file}")
    set(_scm_content "#pragma once\n\n// This file is generated!\n\n")
    set(_scm_collected)
    foreach(dep ${scm_${CMAKE_PROJECT_NAME}_libraries})
        get_target_property(lib_type ${dep} TYPE)
        if(NOT "${lib_type}" STREQUAL "INTERFACE_LIBRARY")
            get_target_property(excluded ${dep} EXCLUDE_FROM_ALL)
            if(excluded)
                continue()
            endif()
        endif()
        string(REGEX REPLACE "-(static|module|shared)" "" dep_short "${dep}")
        list(FIND _scm_collected "${dep_short}" idx)
        if(${idx} EQUAL -1)
            list(APPEND _scm_collected ${dep_short})
            set(_scm_content "${_scm_content}#include <${CMAKE_PROJECT_NAME}/${dep_short}.hpp>\n")
        endif()
    endforeach()

    scm_update_file("${_scm_target_file_abs}" "${_scm_content}")

    scm_add_header_library(
        TARGET                  ${CMAKE_PROJECT_NAME}
        FILES                   ${_scm_target_file}
        DEPENDENCIES            ${_scm_collected}
        SOURCE_DIRECTORY_PREFIX ${PARAM_SOURCE_DIRECTORY_PREFIX}
    )
    if(${scm_file_up_to_date} EQUAL 0)
        _scm_status("     File updated: '${_scm_target_file}'")
    else()
        _scm_status("     File already up to date: '${_scm_target_file}'")
    endif()
endfunction()
