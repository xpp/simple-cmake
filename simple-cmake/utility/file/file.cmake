macro(scm_file cmd)
    if("${cmd}" STREQUAL "MAKE_ABSOLUTE")
        scm_file_make_absolute(${ARGN})
    else()
        list(${cmd} ${ARGN})
    endif()
endmacro()

