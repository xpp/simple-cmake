macro(scm_set_compiler)
    ################################################################################
    #define params
    set(_scm_flags
        NO_FATAL_ERROR
        ONLY_CC
        ONLY_CXX
    )
    set(_scm_single_param
        PREFIX_CC
        PREFIX_CXX
        ENVVERSION
        VERSION
    )
    set(_scm_multi_param)
    cmake_parse_arguments(_scm "${_scm_flags}" "${_scm_single_param}" "${_scm_multi_param}" ${ARGN} )
    ################################################################################
    if(NOT "${_scm_UNPARSED_ARGUMENTS}" STREQUAL "")
        _scm_fatal("some arguments were not parsed: ${_scm_UNPARSED_ARGUMENTS}\nARGN: ${ARGN}")
    endif()
    if("${_scm_ENVVERSION}" STREQUAL "" AND "${_scm_VERSION}" STREQUAL "")
        _scm_fatal("Parameter ENVVERSION or VERSION have to be set \nARGN: ${ARGN}")
    endif()
    if(NOT "${_scm_ENVVERSION}" STREQUAL "" AND NOT "${_scm_VERSION}" STREQUAL "")
        _scm_fatal("Only one of the parameters ENVVERSION ( = '${_scm_ENVVERSION}') or VERSION ( = '${_scm_VERSION}') can be set\nARGN: ${ARGN}")
    endif()

    ################################################################################
    if(NOT _scm_ONLY_CXX)
        set(_scm_c_compiler_name "${_scm_PREFIX_CC}$ENV{${_scm_ENVVERSION}}${_scm_VERSION}")
        execute_process(
            COMMAND which "${_scm_c_compiler_name}"
            OUTPUT_VARIABLE _scm_CMAKE_C_COMPILER
            OUTPUT_STRIP_TRAILING_WHITESPACE)
        if(NOT "" STREQUAL "${_scm_CMAKE_C_COMPILER}")
            set(CMAKE_C_COMPILER ${_scm_CMAKE_C_COMPILER})
            _scm_status("${_scm_c_compiler_name} -> CMAKE_C_COMPILER   : ${CMAKE_C_COMPILER}")
        elseif(_scm_NO_FATAL_ERROR)
            _scm_warning("CMAKE_C_COMPILER '${_scm_c_compiler_name}' NOT FOUND")
        else()
            _scm_fatal("CMAKE_C_COMPILER '${_scm_c_compiler_name}' NOT FOUND")
        endif()
    endif()

    if(NOT _scm_ONLY_CC)
        set(_scm_cxx_compiler_name "${_scm_PREFIX_CXX}$ENV{${_scm_ENVVERSION}}${_scm_VERSION}")
        execute_process(
            COMMAND which "${_scm_cxx_compiler_name}"
            OUTPUT_VARIABLE _scm_CMAKE_CXX_COMPILER
            OUTPUT_STRIP_TRAILING_WHITESPACE)
        if(NOT "" STREQUAL "${_scm_CMAKE_CXX_COMPILER}")
            set(CMAKE_CXX_COMPILER ${_scm_CMAKE_CXX_COMPILER})
            _scm_status("${_scm_c_compiler_name} -> CMAKE_CXX_COMPILER   : ${CMAKE_CXX_COMPILER}")
        elseif(_scm_NO_FATAL_ERROR)
            _scm_warning("CMAKE_CXX_COMPILER '${_scm_cxx_compiler_name}' NOT FOUND")
        else()
            _scm_fatal("CMAKE_CXX_COMPILER '${_scm_cxx_compiler_name}' NOT FOUND")
        endif()
    endif()
endmacro()

macro(scm_set_gcc_compiler)
    scm_set_compiler(PREFIX_CC gcc- PREFIX_CXX g++- ${ARGN})
endmacro()

macro(scm_set_clang_compiler)
    scm_set_compiler(PREFIX_CC clang- PREFIX_CXX clang++- ${ARGN})
endmacro()


