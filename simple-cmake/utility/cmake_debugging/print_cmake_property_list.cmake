function(scm_print_cmake_property_list)
    list(REMOVE_DUPLICATES CMAKE_PROPERTY_LIST)
    _scm_status("CMAKE_PROPERTY_LIST:")
    scm_list_print(PREFIX "[SCM]   * " LIST ${CMAKE_PROPERTY_LIST})
endfunction()

