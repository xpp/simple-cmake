function(scm_print_all_cmake_variables)
    _scm_status("ALL CMAKE VARIABLES:")
    get_cmake_property(_variableNames VARIABLES)
    foreach (_variableName ${_variableNames})
        _scm_status(                "    ${_variableName}")
        scm_list_print(PREFIX "[SCM]         * " LIST ${${_variableName}})
    endforeach()
endfunction()

