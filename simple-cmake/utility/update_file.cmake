macro(scm_update_file file content) #macro since we want to provide scm_file_up_to_date
    set(scm_file_up_to_date 0)
    if(EXISTS "${file}")
        file(SHA512 "${file}" _scm_file_sha)
        string(SHA512 _scm_cont_sha "${content}")
        if("${_scm_cont_sha}" STREQUAL "${_scm_file_sha}")
            set(scm_file_up_to_date 1)
        endif()
    endif()
    #write file
    if(${scm_file_up_to_date} EQUAL 0)
        file(WRITE "${file}" "${content}")

    endif()
endmacro()
