function(scm_add_export exp)
    set(_scm_add_exp_tmp ${scm_export_names} "${exp}")

    list(REMOVE_DUPLICATES _scm_add_exp_tmp)
    scm_list_remove_empty_elements(_scm_add_exp_tmp)
    if(NOT "" STREQUAL "")
        _scm_status("Added export name '${exp}'")
    endif()
    set(scm_export_names "${_scm_add_exp_tmp}"  CACHE INTERNAL   "")

    unset(_scm_add_exp_tmp)
endfunction()
