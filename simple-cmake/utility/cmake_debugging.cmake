
#todo write to chache and only update when var empty
execute_process(COMMAND cmake --help-property-list OUTPUT_VARIABLE CMAKE_PROPERTY_LIST)


STRING(REGEX REPLACE ";" "\\\\;" CMAKE_PROPERTY_LIST "${CMAKE_PROPERTY_LIST}")
STRING(REGEX REPLACE "\n" ";" CMAKE_PROPERTY_LIST "${CMAKE_PROPERTY_LIST}")

include(${CMAKE_CURRENT_LIST_DIR}/cmake_debugging/print_cmake_property_list.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/cmake_debugging/print_cmake_target_properties.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/cmake_debugging/print_cmake_source_properties.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/cmake_debugging/print_cmake_directory_properties.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/cmake_debugging/print_cmake_properties.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/cmake_debugging/print_all_cmake_variables.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/cmake_debugging/print_environment_variables.cmake)

