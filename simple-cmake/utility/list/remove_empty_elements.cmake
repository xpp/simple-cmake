macro(scm_list_remove_empty_elements list)
    set(_scm_lrem_empty_tmp)

    foreach(e ${${list}})
        if(NOT "${e}" STREQUAL "")
            list(APPEND _scm_lrem_empty_tmp ${e})
        endif()
    endforeach()
    set(${list} ${_scm_lrem_empty_tmp})

    unset(_scm_lrem_empty_tmp)
endmacro()
