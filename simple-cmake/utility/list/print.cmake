# [PREFIX prefix_string] [LIST] args...
function(scm_list_print)
    set(flags              )
    set(single_param PREFIX)
    set(multi_param  LIST  )
    cmake_parse_arguments(PARAM "${flags}" "${single_param}" "${multi_param}" ${ARGN})

    foreach (item ${PARAM_LIST} ${PARAM_UNPARSED_ARGUMENTS})
        message(STATUS "${PARAM_PREFIX}${item}")
    endforeach()
endfunction()

