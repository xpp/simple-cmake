# [PREFIX prefix_string] [LIST] args...
macro(scm_list cmd)
    if("${cmd}" STREQUAL "PRINT")
        scm_list_print(${ARGN})
    elseif("${cmd}" STREQUAL "REMOVE_EMPTY_ELEMENTS")
        scm_list_remove_empty_elements(${ARGN})
    else()
        list(${cmd} ${ARGN})
    endif()
endmacro()

