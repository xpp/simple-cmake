function(scm_add_dummy)
    ###############
    #define params#
    ###############
    set(single_param
        TARGET
    )
    set(multi_param
        FILES
    )
    cmake_parse_arguments(PARAM "${flags}" "${single_param}" "${multi_param}" ${ARGN} )
    ##############
    #check params#
    ##############
    if("${PARAM_TARGET}" STREQUAL "")
        _scm_fatal("Parameter TARGET was not set")
    endif()
    ###############
    #config target#
    ###############
    #make the headers show up in the ide (see https://stackoverflow.com/a/29214327)
    add_custom_target(${PARAM_TARGET} SOURCES ${PARAM_FILES})
endfunction()
