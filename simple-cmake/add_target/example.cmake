function(scm_add_example)
    ###############
    #define params#
    ###############
    set(single_param
        TARGET   #target name
        GROUP   #target name
    )
    set(multi_param
        DEPENDENCIES
        FILES
    )
    cmake_parse_arguments(PARAM "${flags}" "${single_param}" "${multi_param}" ${ARGN} )
    ##############
    #check params#
    ##############
    if("${PARAM_TARGET}" STREQUAL "")
        _scm_fatal("Parameter TARGET was not set")
    endif()
    set(PARAM_TARGET example_${PARAM_TARGET})
    if("${PARAM_FILES}" STREQUAL "")
        _scm_fatal("Parameter FILES was not set")
    endif()
    ################
    #verbose output#
    ################
    _scm_status("EXAMPLE ${PARAM_TARGET}")
    _scm_status("     GROUP        ${PARAM_GROUP}")
    _scm_status("     DEPENDENCIES ${PARAM_DEPENDENCIES}")
    ###############
    #config target#
    ###############
    if(scm_build_examples)
        add_executable(${PARAM_TARGET} ${PARAM_FILES})
        set_target_properties(${PARAM_TARGET}
            PROPERTIES
            RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/examples/${PARAM_GROUP}"
        )
        if(NOT "${PARAM_DEPENDENCIES}" STREQUAL "")
            target_link_libraries(${PARAM_TARGET} PRIVATE ${PARAM_DEPENDENCIES})
        endif()
        scm_add_example_as_test(${PARAM_TARGET})
    endif()
    _scm_status()
endfunction()


if(scm_build_examples)
    add_custom_target(collect_example_output
        COMMAND find -mindepth 2 -type d -name example_output | xargs -I% rsync -r --remove-source-files % .
        COMMAND find -type d -empty -delete
        WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
    )
endif()
