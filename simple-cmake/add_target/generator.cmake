function(scm_add_generator)
    ###############
    #define params#
    ###############
    set(flags
        ADD_DUMMY_TARGET
    )
    set(single_param
        DEPENDENCY_OF
        TARGET
    )
    set(multi_param
        COMMAND
        ARGS
        DEPENDS
        COMMENT
        DEPENDENCY_OF
        OUTPUT
    )
    cmake_parse_arguments(PARAM "${flags}" "${single_param}" "${multi_param}" ${ARGN} )
    ##############
    #check params#
    ##############
    if("" STREQUAL "${PARAM_TARGET}")
        _scm_fatal("Parameter TARGET for generator target was not set! ${PARAM_TARGET}")
    endif()
    if("" STREQUAL "${PARAM_COMMAND}")
        _scm_fatal("Parameter COMMAND for generator target '${PARAM_TARGET}' was not set! ${PARAM_COMMAND}")
    endif()
    if("" STREQUAL "${PARAM_DEPENDS}")
        _scm_fatal("Parameter DEPENDS for generator target '${PARAM_TARGET}' was not set! ${PARAM_DEPENDS}")
    endif()
    #############
    #add targets#
    #############
    set(touch_file "${CMAKE_CURRENT_BINARY_DIR}/${PARAM_TARGET}.touch")
    add_custom_command(
        OUTPUT   ${touch_file}
        COMMAND  ${PARAM_COMMAND}   ARGS     ${PARAM_ARGS}
        COMMAND  touch              ARGS    "${touch_file}"
        DEPENDS  ${PARAM_DEPENDS}
        COMMENT "${PARAM_COMMENT}"
    )
    add_custom_target(
        ${PARAM_TARGET}
        DEPENDS ${touch_file}
    )

    foreach(dependency ${PARAM_DEPENDENCY_OF})
        add_dependencies(${dependency} ${PARAM_TARGET})
    endforeach()

    foreach(output ${PARAM_OUTPUT})
        if(NOT EXISTS "${output}")
            execute_process(COMMAND touch "${output}")
        endif()
    endforeach()
    if(PARAM_ADD_DUMMY_TARGET AND NOT "" STREQUAL "${PARAM_OUTPUT}")
        scm_add_dummy(TARGET _${PARAM_TARGET} FILES ${PARAM_OUTPUT})
    endif()
endfunction()
