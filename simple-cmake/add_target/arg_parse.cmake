
macro(_scm_parse_lib_args_exclude_from_all)
    set(_scm_tmp_exclude_from_all FALSE)
    if(NOT _scm_tmp_exclude_from_all AND NOT "${PARAM_EXCLUDE_IF_ANY_TRUE}" STREQUAL "")
        foreach(var ${PARAM_EXCLUDE_IF_ANY_TRUE})
            if(${var})
                set(_scm_tmp_exclude_from_all TRUE)
                break()
            endif()
        endforeach()
    endif()
    if(NOT _scm_tmp_exclude_from_all AND NOT "${PARAM_EXCLUDE_IF_ANY_FALSE}" STREQUAL "")
        foreach(var ${PARAM_EXCLUDE_IF_ANY_FALSE})
            if(NOT ${var})
                set(_scm_tmp_exclude_from_all TRUE)
                break()
            endif()
        endforeach()
    endif()
    if(NOT _scm_tmp_exclude_from_all AND NOT "${PARAM_EXCLUDE_IF_ALL_TRUE}" STREQUAL "")
        set(_scm_tmp_exclude_from_all_sub TRUE)
        foreach(var ${PARAM_EXCLUDE_IF_ALL_TRUE})
            if(NOT ${var})
                set(_scm_tmp_exclude_from_all_sub FALSE)
                break()
            endif()
        endforeach()
        if(_scm_tmp_exclude_from_all_sub)
            set(_scm_tmp_exclude_from_all TRUE)
        endif()
    endif()
    if(NOT _scm_tmp_exclude_from_all AND NOT "${PARAM_EXCLUDE_IF_ALL_FALSE}" STREQUAL "")
        set(_scm_tmp_exclude_from_all_sub TRUE)
        foreach(var ${PARAM_EXCLUDE_IF_ALL_FALSE})
            if(${var})
                set(_scm_tmp_exclude_from_all_sub FALSE)
                break()
            endif()
        endforeach()
        if(_scm_tmp_exclude_from_all_sub)
            set(_scm_tmp_exclude_from_all TRUE)
        endif()
    endif()
endmacro()

#wip not used
#TODO make lib args for header only and other libs more uniform
macro(_scm_parse_lib_args)
    ############################################################################
    #define params
    set(multi_param
        #UNORDERED
            TARGET                              # single
            EXPORT                              # single
            GENERATE_SUBDIR_HEADERS             # flag
            FILES                               # multi

        #INSTALL_DIRECTORY
            INSTALL_DIRECTORY                   # HEADERS | LIBRARY | ARCHIVE

            INSTALL_DIRECTORY_HEADERS           #[...] [PUBLIC [...]] [PRIVATE [...]]

            INSTALL_DIRECTORY_HEADERS_PUBLIC    # single
            INSTALL_DIRECTORY_HEADERS_PRIVATE   # single

            INSTALL_DIRECTORY_LIBRARY           # single
            INSTALL_DIRECTORY_ARCHIVE           # single
            INSTALL_DIRECTORY_RUNTIME           # single
            INSTALL_DIRECTORY_OBJECTS           # single
            INSTALL_DIRECTORY_RESOURCE          # single

        #INCLUDE_DIRECTORIES
            INCLUDE_DIRECTORIES                 # multi
            INCLUDE_DIRECTORIES_PUBLIC          # multi
            INCLUDE_DIRECTORIES_INTERFACE       # multi
            INCLUDE_DIRECTORIES_PRIVATE         # multi

        #DEPENDENCIES
            DEPENDENCIES                        # multi
            DEPENDENCIES_PUBLIC                 # multi
            DEPENDENCIES_INTERFACE              # multi
            DEPENDENCIES_PRIVATE                # multi
    )

    cmake_parse_arguments(_scm "" "" "${multi_param}" ${ARGN})

    ############################################################################
    ################################### vars ###################################
    ############################################################################
    set(_error_string "\nParsed keywords: ${multi_param}\nParameters: ${ARGN}")
    ############################################################################
    ###################### parse group UNPARSED_ARGUMENTS ######################
    ############################################################################
    #UNPARSED_ARGUMENTS
    if(_scm_UNPARSED_ARGUMENTS)
        _scm_fatal(
            "Some parameters were not parsed!\nUnparsed:\
            ${_scm_UNPARSED_ARGUMENTS}\
            ${_error_string}")
    endif()
    ############################################################################
    ########################### parse group UNORDERED ##########################
    ############################################################################
    unset(_scm_trg)
    unset(_scm_exp)
    unset(_scm_gen_sub_hdr)
    unset(_scm_fs)

    #TARGET
    if(_scm_TARGET)
        list(LENGTH _scm_TARGET _scm_list_len)
        if(NOT ${_scm_list_len} EQUAL 1)
            _scm_fatal("Parameter TARGET is not set exactly once! \
                \nValues: ${_scm_TARGET} \
                ${_error_string}")
        endif()
        set(_scm_trg ${_scm_TARGET})
    else()
        _scm_fatal("Parameter TARGET is not set exactly once!${_error_string}")
    endif()

    #EXPORT
    if(_scm_EXPORT)
        list(LENGTH _scm_EXPORT _scm_list_len)
        if(NOT ${_scm_list_len} EQUAL 1)
            _scm_fatal("Parameter EXPORT is not set exactly once!\
                \nValues: ${_scm_EXPORT}\
                ${_error_string}")
        endif()
        set(_scm_exp ${_scm_EXPORT})
    else()
        set(_scm_exp "${scm_default_export}")
    endif()

    #GENERATE_SUBDIR_HEADERS
    foreach(path ${_scm_GENERATE_SUBDIR_HEADERS})
        scm_file_make_absolute(abs "${path}")
        list(APPEND _scm_gen_sub_hdr "${abs}")
        if(NOT IS_DIRECTORY "${abs}")
            _scm_fatal(
                "One path given to GENERATE_SUBDIR_HEADERS is no directory: \
                ${path}\
                \nAll paths: ${_scm_GENERATE_SUBDIR_HEADERS}\
                ${_error_string}")
        endif()
    endforeach()

    #FILES
    foreach(path ${_scm_FILES})
        scm_file_make_absolute(abs "${path}")
        list(APPEND _scm_fs "${abs}")
        if( NOT EXISTS "${abs}" OR IS_DIRECTORY "${abs}")
            _scm_fatal(
                "One path given to FILES is no file: ${path}\
                \nAll paths: ${_scm_FILES}\
                ${_error_string}")
        endif()
    endforeach()
    ############################################################################
    ####################### parse group INSTALL_DIRECTORY ######################
    ############################################################################
    unset(_scm_ins_dir_hdr)
    unset(_scm_ins_dir_hdr_prv)
    unset(_scm_ins_dir_lib)
    unset(_scm_ins_dir_arc)
    unset(_scm_ins_dir_run)
    unset(_scm_ins_dir_obj)
    unset(_scm_ins_dir_res)
    cmake_parse_arguments(_scm_ins
        "" "" "HEADERS;LIBRARY;ARCHIVE;RUNTIME;OBJECTS;RESOURCE"
        ${_scm_INSTALL_DIRECTORY}) ########unparsed
    cmake_parse_arguments(_scm_ins_hdr
        "" "" "PUBLIC;PRIVATE"
        ${_scm_INSTALL_DIRECTORY_HEADERS} ${_scm_ins_HEADERS}) ########unparsed
    ############################################################################
    macro(_scm_ihdr vis_upper vis_lower var default)
        set(${var}
            ${_scm_INSTALL_DIRECTORY_HEADERS_${vis_upper}}
            ${_scm_ins_hdr_${vis_upper}})
        list(LENGTH ${var} _scm_list_len)
        if(${_scm_list_len} EQUAL 0)
            set(${var} ${default})
        elseif(NOT ${_scm_list_len} EQUAL 1)
            _scm_fatal(
                "Multiple install directories for ${vis_lower} headers!\
                \nDirs: ${${var}}\
                ${_error_string}")
        endif()
    endmacro()
    _scm_ihdr(PUBLIC  public  _scm_ins_dir_hdr     "${scm_install_dir_header}"        )
    _scm_ihdr(PRIVATE private _scm_ins_dir_hdr_prv "${scm_install_dir_header_private}")
    ############################################################################
    macro(_scm_inrm component var plural default)
        set(${var}
            ${_scm_INSTALL_DIRECTORY_${component}}
            ${_scm_ins_${component}})
        list(LENGTH ${var} _scm_list_len)
        if(${_scm_list_len} EQUAL 0)
            set(${var} ${default})
        elseif(NOT ${_scm_list_len} EQUAL 1)
            _scm_fatal(
                "Multiple install directories for ${plural}!\
                \nDirs: ${${var}}\
                ${_error_string}")
        endif()
    endmacro()
    _scm_inrm(LIBRARY  _scm_ins_dir_lib libraries "${scm_install_dir_library}" )
    _scm_inrm(ARCHIVE  _scm_ins_dir_arc archives  "${scm_install_dir_archive}" )
    _scm_inrm(RUNTIME  _scm_ins_dir_run runtimes  "${scm_install_dir_runtime}" )
    _scm_inrm(OBJECTS  _scm_ins_dir_obj objects   "${scm_install_dir_objects}" )
    _scm_inrm(RESOURCE _scm_ins_dir_res resources "${scm_install_dir_resource}")
    ############################################################################
    ###################### parse group INCLUDE_DIRECTORIES #####################
    ############################################################################
    unset(_scm_inc_dir)
    unset(_scm_inc_dir_pub)
    unset(_scm_inc_dir_prv)
    unset(_scm_inc_dir_int)

    unset(_scm_inc_dir_sys)
    unset(_scm_inc_dir_sys_pub)
    unset(_scm_inc_dir_sys_prv)
    unset(_scm_inc_dir_sys_int)
    cmake_parse_arguments(_scm_inc
        "" "" "PUBLIC;PRIVATE;INTERFACE"
        ${_scm_INCLUDE_DIRECTORIES})
    ############################################################################
    macro(_scm_inc suf_lower a b)
        cmake_parse_arguments(_scm_inc_sub_a "" "" "SYSTEM" ${a})
        cmake_parse_arguments(_scm_inc_sub_b "" "" "SYSTEM" ${b})
        set(_scm_inc_dir${suf_lower}     ${_scm_inc_sub_a_UNPARSED_ARGUMENTS}
                                         ${_scm_inc_sub_b_UNPARSED_ARGUMENTS})
        set(_scm_inc_dir_sys${suf_lower} ${_scm_inc_sub_a_SYSTEM}
                                         ${_scm_inc_sub_b_SYSTEM})
    endmacro()
    _scm_inc(_pub "${_scm_INCLUDE_DIRECTORIES_PUBLIC}"    "${_scm_inc_PUBLIC}"   )
    _scm_inc(_prv "${_scm_INCLUDE_DIRECTORIES_PRIVATE}"   "${_scm_inc_PRIVATE}"  )
    _scm_inc(_int "${_scm_INCLUDE_DIRECTORIES_INTERFACE}" "${_scm_inc_INTERFACE}")
    _scm_inc(""   "${_scm_inc_UNPARSED_ARGUMENTS}"        ""                     )
    ############################################################################
    ######################### parse group DEPENDENCIES #########################
    ############################################################################
    unset(_scm_dep)
    unset(_scm_dep_pub)
    unset(_scm_dep_prv)
    unset(_scm_dep_int)
    cmake_parse_arguments(_scm_dep
        "" "" "PUBLIC;PRIVATE;INTERFACE"
        ${_scm_DEPENDENCIES})
    ############################################################################
    set(_scm_dep     ${_scm_dep_UNPARSED_ARGUMENTS}                            )
    set(_scm_dep_pub ${_scm_DEPENDENCIES_PUBLIC}    ${_scm_dep_PUBLIC}         )
    set(_scm_dep_prv ${_scm_DEPENDENCIES_PRIVATE}   ${_scm_dep_PRIVATE}        )
    set(_scm_dep_int ${_scm_DEPENDENCIES_INTERFACE} ${_scm_dep_INTERFACE}      )
endmacro()
