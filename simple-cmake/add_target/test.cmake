if(scm_build_tests)
    enable_testing()
endif()

function(scm_add_test)
    ###############
    #define params#
    ###############
    set(single_param
        TARGET   #target name
        GROUP   #target name
    )
    set(multi_param
        DEPENDENCIES
        FILES
    )
    cmake_parse_arguments(PARAM "${flags}" "${single_param}" "${multi_param}" ${ARGN} )
    ##############
    #check params#
    ##############
    if("${PARAM_TARGET}" STREQUAL "")
        _scm_fatal("Parameter TARGET was not set")
    endif()
    set(PARAM_TARGET test_${PARAM_GROUP}_${PARAM_TARGET})
    if("${PARAM_FILES}" STREQUAL "")
        _scm_fatal("Parameter FILES was not set")
    endif()
    ################
    #verbose output#
    ################
    _scm_status("TEST ${PARAM_TARGET}")
    _scm_status("     GROUP        ${PARAM_GROUP}")
    _scm_status("     DEPENDENCIES ${PARAM_DEPENDENCIES}")
    ###############
    #config target#
    ###############
    if(scm_build_tests)
        add_executable(${PARAM_TARGET} ${PARAM_FILES})

        set_target_properties(${PARAM_TARGET}
            PROPERTIES
            RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/tests/${PARAM_GROUP}"
        )
        if(NOT "${PARAM_DEPENDENCIES}" STREQUAL "")
            foreach(dep ${PARAM_DEPENDENCIES})
                #maybe the "target" is not a real target but wants to switch between static etc
                set(actual_dep ${dep})
                if(NOT TARGET ${actual_dep})
                    if(TARGET ${dep}-static)
                        set(actual_dep ${dep}-static)
                    elseif(TARGET ${dep}-shared)
                        set(actual_dep ${dep}-shared)
                    elseif(TARGET ${dep}-shared)
                        set(actual_dep ${dep}-module)
                    endif()
                endif()
                target_link_libraries(${PARAM_TARGET} PRIVATE ${actual_dep})
            endforeach()
        endif()

        if(NOT Boost_UNIT_TEST_FRAMEWORK_FOUND)
            _scm_fatal("Boost component unit_test_framework was not found")
        endif()

        target_include_directories(${PARAM_TARGET} PRIVATE ${Boost_INCLUDE_DIRS})
        target_compile_definitions(${PARAM_TARGET} PRIVATE "BOOST_TEST_DYN_LINK=1")
        target_link_libraries(${PARAM_TARGET} PRIVATE ${Boost_UNIT_TEST_FRAMEWORK_LIBRARY})
        add_test(NAME ${PARAM_TARGET} COMMAND ${PARAM_TARGET})
        scm_add_coverage(${PARAM_TARGET})
    endif()
    _scm_status()
endfunction()


function(scm_add_example_as_test target)
    if(scm_build_tests)
        add_test(NAME ${target} COMMAND ${target})
        scm_add_coverage(${target})
        _scm_status("     ADD AS TEST  ${target}")
    endif()
endfunction()
