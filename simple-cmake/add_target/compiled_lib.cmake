
function(scm_add_library)
    _scm_add_library_base(STATIC SHARED ${ARGN})
endfunction()

function(scm_add_static_library)
    _scm_add_library_base(STATIC ${ARGN})
endfunction()

function(scm_add_shared_library)
    _scm_add_library_base(SHARED ${ARGN})
endfunction()

function(scm_add_module_library)
    _scm_add_library_base(MODULE ${ARGN})
endfunction()

function(_scm_add_library_base)
    #define params
    set(flags
        STATIC
        SHARED
        MODULE)
    set(single_param
        TARGET
        EXPORT
        GENERATE_SUBDIR_HEADERS

        INSTALL_DIRECTORY_HEADERS
        INSTALL_DIRECTORY_HEADERS_PRIVATE

        INSTALL_DIRECTORY_LIBRARY
        INSTALL_DIRECTORY_ARCHIVE

        SOURCE_DIRECTORY_PREFIX
    )
    set(multi_param

        INCLUDE_DIRECTORIES
        INCLUDE_DIRECTORIES_PUBLIC
        INCLUDE_DIRECTORIES_INTERFACE
        INCLUDE_DIRECTORIES_PRIVATE

        DEPENDENCIES
        DEPENDENCIES_PUBLIC
        DEPENDENCIES_INTERFACE
        DEPENDENCIES_PRIVATE
        DEPENDENCIES_ORDER

        FILES

        EXCLUDE_IF_ANY_TRUE
        EXCLUDE_IF_ANY_FALSE
        EXCLUDE_IF_ALL_TRUE
        EXCLUDE_IF_ALL_FALSE
    )
    cmake_parse_arguments(PARAM "${flags}" "${single_param}" "${multi_param}" ${ARGN} )
    ################################################################################
    #check params
    if(NOT "${PARAM_UNPARSED_ARGUMENTS}" STREQUAL "")
        _scm_fatal("some arguments were not parsed: ${PARAM_UNPARSED_ARGUMENTS}\nARGN: ${ARGN}")
    endif()

    if(NOT ${PARAM_STATIC} AND NOT ${PARAM_SHARED} AND NOT ${PARAM_MODULE})
        _scm_fatal("Neither STATIC, nor SHARED nor MODULE was set as library type")
    endif()
    set(LIB_TYPES)
    foreach(t STATIC SHARED MODULE)
        if(${PARAM_${t}})
            list(APPEND LIB_TYPES ${t})
        endif()
    endforeach()

    if("${PARAM_TARGET}" STREQUAL "")
        _scm_fatal("Parameter TARGET was not set")
    endif()

    if("${PARAM_FILES}" STREQUAL "")
        _scm_fatal("Parameter FILES was not set")
    endif()

    set(_scm_source_dir ${CMAKE_CURRENT_LIST_DIR})
    if(NOT "${PARAM_SOURCE_DIRECTORY_PREFIX}" STREQUAL "")
        string(APPEND _scm_source_dir "/${PARAM_SOURCE_DIRECTORY_PREFIX}")
        set(_scm_tmp ${PARAM_FILES})
        set(PARAM_FILES)
        foreach(f ${_scm_tmp})
            list(APPEND PARAM_FILES "${_scm_source_dir}/${f}")
        endforeach()
    endif()

    _scm_parse_lib_args_exclude_from_all()
    ################################################################################
    #copy defaults
    scm_set_if_undefined(PARAM_EXPORT                            ${scm_default_export})
    scm_add_export(${PARAM_EXPORT})

    scm_set_if_undefined(PARAM_INSTALL_DIRECTORY_HEADERS         ${scm_install_dir_header})
    scm_set_if_undefined(PARAM_INSTALL_DIRECTORY_HEADERS_PRIVATE ${scm_install_dir_header_private})

    scm_set_if_undefined(PARAM_INSTALL_DIRECTORY_LIBRARY         ${scm_install_dir_library})
    scm_set_if_undefined(PARAM_INSTALL_DIRECTORY_ARCHIVE         ${scm_install_dir_archive})

    set(PARAM_INSTALL_DIRECTORY_HEADERS         ${PARAM_INSTALL_DIRECTORY_HEADERS}/_${PARAM_TARGET}/)
    set(PARAM_INSTALL_DIRECTORY_HEADERS_PRIVATE ${PARAM_INSTALL_DIRECTORY_HEADERS_PRIVATE}/_${PARAM_TARGET}/)

    if(NOT "${PARAM_INCLUDE_DIRECTORIES}" STREQUAL "")
        list(APPEND PARAM_INCLUDE_DIRECTORIES_PUBLIC ${PARAM_INCLUDE_DIRECTORIES})
    endif()

    if(NOT "${PARAM_DEPENDENCIES}" STREQUAL "")
        list(APPEND PARAM_DEPENDENCIES_PUBLIC ${PARAM_DEPENDENCIES})
    endif()
    ################################################################################
    #verbose output
    string(SUBSTRING "[${LIB_TYPES}]                 " 0 23 lib_ts_str)
    _scm_status("LIB ${lib_ts_str}: ${PARAM_TARGET} (from '${_scm_source_dir}')")
    _scm_status("     EXPORT                : ${PARAM_EXPORT}")

    macro(msg pre post)
        if(NOT "${post}" STREQUAL "")
            _scm_status("${pre} ${post}")
        endif()
    endmacro()

    if(NOT "" STREQUAL "${PARAM_INSTALL_DIRECTORY_LIBRARY}" OR
       NOT "" STREQUAL "${PARAM_INSTALL_DIRECTORY_ARCHIVE}" OR
       NOT "" STREQUAL "${PARAM_INSTALL_DIRECTORY_HEADERS}" OR
       NOT "" STREQUAL "${PARAM_INSTALL_DIRECTORY_HEADERS_PRIVATE}"
    )
        _scm_status("     INSTALL DIRECTORIES")
        msg("         LIBRARY           :" "${PARAM_INSTALL_DIRECTORY_LIBRARY}")
        msg("         ARCHIVE           :" "${PARAM_INSTALL_DIRECTORY_ARCHIVE}")
        msg("         HEADERS (PUBLIC)  :" "${PARAM_INSTALL_DIRECTORY_HEADERS}")
        msg("         HEADERS (PRIVATE) :" "${PARAM_INSTALL_DIRECTORY_HEADERS_PRIVATE}")
    endif()

    if(NOT "" STREQUAL "${PARAM_INCLUDE_DIRECTORIES_PUBLIC}" OR
       NOT "" STREQUAL "${PARAM_INCLUDE_DIRECTORIES_INTERFACE}" OR
       NOT "" STREQUAL "${PARAM_INCLUDE_DIRECTORIES_PRIVATE}"
    )
        _scm_status("     INCLUDE DIRECTORIES")
        msg("         PUBLIC            :" "${PARAM_INCLUDE_DIRECTORIES_PUBLIC}")
        msg("         INTERFACE         :" "${PARAM_INCLUDE_DIRECTORIES_INTERFACE}")
        msg("         PRIVATE           :" "${PARAM_INCLUDE_DIRECTORIES_PRIVATE}")
    endif()

    if(NOT "" STREQUAL "${PARAM_DEPENDENCIES_PUBLIC}" OR
       NOT "" STREQUAL "${PARAM_DEPENDENCIES_INTERFACE}" OR
       NOT "" STREQUAL "${PARAM_DEPENDENCIES_PRIVATE}" OR
       NOT "" STREQUAL "${PARAM_DEPENDENCIES_ORDER}"
    )
        _scm_status("     DEPENDENCIES")
        msg("         PUBLIC            :" "${PARAM_DEPENDENCIES_PUBLIC}")
        msg("         INTERFACE         :" "${PARAM_DEPENDENCIES_INTERFACE}")
        msg("         PRIVATE           :" "${PARAM_DEPENDENCIES_PRIVATE}")
        msg("         ORDER             :" "${PARAM_DEPENDENCIES_ORDER}")
    endif()
    ################################################################################
    #generate headers
    if(NOT "${PARAM_GENERATE_SUBDIR_HEADERS}" STREQUAL "")
        scm_generate_subdir_headers(
            ${_scm_source_dir}/${PARAM_GENERATE_SUBDIR_HEADERS}
            ${_scm_source_dir}
            PARAM_FILES
        )
    endif()
    ################################################################################
    #config targets (static/shared/module)
    #add libraties for all used types (static shared module)
    foreach(LIB_TYPE ${LIB_TYPES})
        string(TOLOWER "${LIB_TYPE}" lib_type)
        set(ACTUAL_TARGET "${PARAM_TARGET}-${lib_type}")
        if(_scm_tmp_exclude_from_all)
            add_library(${ACTUAL_TARGET} ${LIB_TYPE} EXCLUDE_FROM_ALL ${PARAM_FILES})
        else()
            add_library(${ACTUAL_TARGET} ${LIB_TYPE} ${PARAM_FILES})
        endif()
        if(NOT "${PARAM_DEPENDENCIES_ORDER}" STREQUAL "")
            add_dependencies(${ACTUAL_TARGET} ${PARAM_DEPENDENCIES_ORDER})
        endif()
        # shared libraries need PIC (and it is always better to use pic)
        set_property(TARGET ${ACTUAL_TARGET} PROPERTY POSITION_INDEPENDENT_CODE 1)
        #include dirs
        target_include_directories(${ACTUAL_TARGET} PUBLIC
            $<BUILD_INTERFACE:${_scm_source_dir}>
            $<INSTALL_INTERFACE:${PARAM_INSTALL_DIRECTORY_HEADERS}>
        )
        foreach(vis PUBLIC INTERFACE PRIVATE)
            if(NOT "${PARAM_INCLUDE_DIRECTORIES_${vis}}" STREQUAL "")
                target_include_directories(
                    ${ACTUAL_TARGET} SYSTEM
                    ${vis} ${PARAM_INCLUDE_DIRECTORIES_${vis}}
                )
            endif()
            if(NOT _scm_tmp_exclude_from_all)
                foreach(dep ${PARAM_DEPENDENCIES_${vis}})
                    #maybe the "target is not a real target but wants to switch between static etc
                    set(actual_dep ${dep})
                    if(NOT TARGET ${actual_dep})
                        get_target_property(target_type ${ACTUAL_TARGET} TYPE)
                        if(    "${target_type}" STREQUAL "STATIC_LIBRARY")
                            if(TARGET ${dep}-static)
                                set(actual_dep ${dep}-static)
                            endif()
                        elseif("${target_type}" STREQUAL "MODULE_LIBRARY")
                            if(TARGET ${dep}-module)
                                set(actual_dep ${dep}-module)
                            elseif(TARGET ${dep}-shared)
                                set(actual_dep ${dep}-shared)
                            endif()
                        elseif("${target_type}" STREQUAL "SHARED_LIBRARY")
                            if(TARGET ${dep}-shared)
                                set(actual_dep ${dep}-shared)
                            endif()
                        else()
                            _scm_fatal("The target type of a lib is '${target_type}'. something in simple-cmake is broken")
                        endif()
                    endif()
                    target_link_libraries(${ACTUAL_TARGET} ${vis} ${actual_dep})
                endforeach()
            endif()
        endforeach()
        set_target_properties(${ACTUAL_TARGET}
            PROPERTIES
            RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin"
            ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib"
            LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib"
            OUTPUT_NAME ${CMAKE_PROJECT_NAME}_${PARAM_TARGET}
        )
        scm_add_coverage(${ACTUAL_TARGET})
        _scm_register_library_target(${ACTUAL_TARGET})
    endforeach()
    ################################################################################
    #export and install
    foreach(LIB_TYPE ${LIB_TYPES})
        string(TOLOWER "${LIB_TYPE}" lib_type)
        set(ACTUAL_TARGET "${PARAM_TARGET}-${lib_type}")
        install(
            TARGETS                     ${ACTUAL_TARGET}
            EXPORT                      ${PARAM_EXPORT}

            ARCHIVE         DESTINATION ${PARAM_INSTALL_DIRECTORY_ARCHIVE}
            LIBRARY         DESTINATION ${PARAM_INSTALL_DIRECTORY_LIBRARY}

            PUBLIC_HEADER   DESTINATION ${PARAM_INSTALL_DIRECTORY_HEADERS}
            PRIVATE_HEADER  DESTINATION ${PARAM_INSTALL_DIRECTORY_HEADERS_PRIVATE}
        )
    endforeach()
    _scm_status()
endfunction()
