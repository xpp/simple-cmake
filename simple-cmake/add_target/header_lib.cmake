function(scm_add_header_library)
    ################################################################################
    #define params
    set(flags
    )
    set(single_param
        TARGET
        EXPORT
        GENERATE_SUBDIR_HEADERS

        INSTALL_DIRECTORY_HEADERS
        INSTALL_DIRECTORY_HEADERS_PRIVATE

        SOURCE_DIRECTORY_PREFIX
    )
    set(multi_param

        INCLUDE_DIRECTORIES
        INCLUDE_DIRECTORIES_PUBLIC
        PARAM_INCLUDE_DIRECTORIES_INTERFACE

        DEPENDENCIES
        DEPENDENCIES_PUBLIC
        DEPENDENCIES_INTERFACE
        DEPENDENCIES_ORDER

        FILES

        #EXCLUDE_IF_ANY_TRUE
        #EXCLUDE_IF_ANY_FALSE
        #EXCLUDE_IF_ALL_TRUE
        #EXCLUDE_IF_ALL_FALSE
    )
    cmake_parse_arguments(PARAM "${flags}" "${single_param}" "${multi_param}" ${ARGN} )
    ################################################################################
    #check params
    if(NOT "${PARAM_UNPARSED_ARGUMENTS}" STREQUAL "")
        _scm_fatal("some arguments were not parsed: ${PARAM_UNPARSED_ARGUMENTS}\nARGN: ${ARGN}")
    endif()

    if("${PARAM_TARGET}" STREQUAL "")
        _scm_fatal("Parameter TARGET was not set")
    endif()

    if("${PARAM_FILES}" STREQUAL "")
        _scm_fatal("Parameter FILES was not set")
    endif()

    set(_scm_source_dir ${CMAKE_CURRENT_LIST_DIR})
    if(NOT "${PARAM_SOURCE_DIRECTORY_PREFIX}" STREQUAL "")
        string(APPEND _scm_source_dir "/${PARAM_SOURCE_DIRECTORY_PREFIX}")
        set(_scm_tmp ${PARAM_FILES})
        set(PARAM_FILES)
        foreach(f ${_scm_tmp})
            list(APPEND PARAM_FILES "${_scm_source_dir}/${f}")
        endforeach()
    endif()
    ################################################################################
    #copy defaults
    scm_set_if_undefined(PARAM_EXPORT                            ${scm_default_export})
    scm_add_export(${PARAM_EXPORT})

    scm_set_if_undefined(PARAM_INSTALL_DIRECTORY_HEADERS         ${scm_install_dir_header})
    scm_set_if_undefined(PARAM_INSTALL_DIRECTORY_HEADERS_PRIVATE ${scm_install_dir_header_private})

    set(PARAM_INSTALL_DIRECTORY_HEADERS         ${PARAM_INSTALL_DIRECTORY_HEADERS}/_${PARAM_TARGET}/)
    set(PARAM_INSTALL_DIRECTORY_HEADERS_PRIVATE ${PARAM_INSTALL_DIRECTORY_HEADERS_PRIVATE}/_${PARAM_TARGET}/)

    if(NOT "${PARAM_INCLUDE_DIRECTORIES}" STREQUAL "")
        list(APPEND PARAM_INCLUDE_DIRECTORIES_PUBLIC ${PARAM_INCLUDE_DIRECTORIES})
    endif()
    if(NOT "${PARAM_INCLUDE_DIRECTORIES_INTERFACE}" STREQUAL "")
        list(APPEND PARAM_INCLUDE_DIRECTORIES_PUBLIC ${PARAM_INCLUDE_DIRECTORIES_INTERFACE})
    endif()

    if(NOT "${PARAM_DEPENDENCIES}" STREQUAL "")
        list(APPEND PARAM_DEPENDENCIES_PUBLIC ${PARAM_DEPENDENCIES})
    endif()
    if(NOT "${PARAM_DEPENDENCIES_INTERFACE}" STREQUAL "")
        list(APPEND PARAM_DEPENDENCIES_PUBLIC ${PARAM_DEPENDENCIES_INTERFACE})
    endif()

    _scm_parse_lib_args_exclude_from_all()
    ################################################################################
    #verbose output
    _scm_status("LIB [HEADER]               : ${PARAM_TARGET}  (from '${_scm_source_dir}')")
    _scm_status("     EXPORT                : ${PARAM_EXPORT}")

    macro(msg pre post)
        if(NOT "${post}" STREQUAL "")
            _scm_status("${pre} ${post}")
        endif()
    endmacro()

    if(NOT "" STREQUAL "${PARAM_INSTALL_DIRECTORY_HEADERS}" OR
       NOT "" STREQUAL "${PARAM_INSTALL_DIRECTORY_HEADERS_PRIVATE}"
    )
        _scm_status("     INSTALL DIRECTORIES")
        msg("         HEADERS (PUBLIC)  :" "${PARAM_INSTALL_DIRECTORY_HEADERS}")
        msg("         HEADERS (PRIVATE) :" "${PARAM_INSTALL_DIRECTORY_HEADERS_PRIVATE}")
    endif()

    msg("     INCLUDE               :" "${PARAM_INCLUDE_DIRECTORIES}")
    msg("     DEPENDENCIES          :" "${PARAM_DEPENDENCIES}")
    ################################################################################
    #config target
    if(NOT "${PARAM_GENERATE_SUBDIR_HEADERS}" STREQUAL "")
        scm_generate_subdir_headers(
            ${_scm_source_dir}/${PARAM_GENERATE_SUBDIR_HEADERS}
            ${_scm_source_dir}
            PARAM_FILES
        )
    endif()

    add_library(${PARAM_TARGET} INTERFACE)
    #TODO _scm_tmp_exclude_from_all
    _scm_register_library_target(${PARAM_TARGET})

    if(NOT "${PARAM_DEPENDENCIES_ORDER}" STREQUAL "")
        add_dependencies("${PARAM_TARGET}" ${PARAM_DEPENDENCIES_ORDER})
    endif()

    target_include_directories(${PARAM_TARGET} INTERFACE
        $<BUILD_INTERFACE:${_scm_source_dir}>
        $<INSTALL_INTERFACE:${PARAM_INSTALL_DIRECTORY}>
    )

    if(NOT "${PARAM_INCLUDE_DIRECTORIES_PUBLIC}" STREQUAL "")
        target_include_directories(
            ${PARAM_TARGET} SYSTEM
            INTERFACE ${PARAM_INCLUDE_DIRECTORIES_PUBLIC})
    endif()

    foreach(dep ${PARAM_DEPENDENCIES_PUBLIC})
        #maybe the "target" is not a real target but wants to switch between static etc
        set(actual_dep ${dep})
        if(NOT TARGET ${actual_dep})
            if(TARGET ${dep}-static)
                set(actual_dep ${dep}-static)
            elseif(TARGET ${dep}-shared)
                set(actual_dep ${dep}-shared)
            elseif(TARGET ${dep}-shared)
                set(actual_dep ${dep}-module)
            endif()
        endif()
        target_link_libraries(${PARAM_TARGET} INTERFACE ${actual_dep})
    endforeach()
    scm_add_dummy(TARGET _${PARAM_TARGET} FILES ${PARAM_FILES})
    ####################
    #export and install#
    ####################
    install(
        TARGETS                    ${PARAM_TARGET}
        EXPORT                     ${PARAM_EXPORT}
        PUBLIC_HEADER  DESTINATION ${PARAM_INSTALL_DIRECTORY_HEADERS}
        PRIVATE_HEADER DESTINATION ${PARAM_INSTALL_DIRECTORY_HEADERS_PRIVATE}
    )
    _scm_status()
endfunction()
