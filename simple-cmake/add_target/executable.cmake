#TODO
function(scm_add_executable)
    ###############
    #define params#
    ###############
    set(single_param
        TARGET   #target name
        GROUP   #target name
    )
    set(multi_param
        DEPENDENCIES
        FILES

        EXCLUDE_IF_ANY_TRUE
        EXCLUDE_IF_ANY_FALSE
        EXCLUDE_IF_ALL_TRUE
        EXCLUDE_IF_ALL_FALSE
    )
    cmake_parse_arguments(PARAM "${flags}" "${single_param}" "${multi_param}" ${ARGN} )
    ##############
    #check params#
    ##############
    if("${PARAM_TARGET}" STREQUAL "")
        _scm_fatal("Parameter TARGET was not set")
    endif()
    if("${PARAM_FILES}" STREQUAL "")
        _scm_fatal("Parameter FILES was not set")
    endif()

    _scm_parse_lib_args_exclude_from_all()
    ################
    #verbose output#
    ################
    _scm_status("EXECUTABLE ${PARAM_TARGET}")
    _scm_status("     GROUP        ${PARAM_GROUP}")
    _scm_status("     DEPENDENCIES ${PARAM_DEPENDENCIES}")
    ###############
    #config target#
    ###############
    if(_scm_tmp_exclude_from_all)
        add_executable(${PARAM_TARGET} EXCLUDE_FROM_ALL ${PARAM_FILES})
    else()
        add_executable(${PARAM_TARGET} ${PARAM_FILES})
    endif()

    set_target_properties(${PARAM_TARGET}
        PROPERTIES
        RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin"
        ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib"
        LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib"
    )
    if(NOT _scm_tmp_exclude_from_all)
        if(NOT "${PARAM_DEPENDENCIES}" STREQUAL "")
            foreach(dep ${PARAM_DEPENDENCIES})
                #maybe the "target" is not a real target but wants to switch between static etc
                set(actual_dep ${dep})
                if(NOT TARGET ${actual_dep})
                    if(TARGET ${dep}-static)
                        set(actual_dep ${dep}-static)
                    elseif(TARGET ${dep}-shared)
                        set(actual_dep ${dep}-shared)
                    elseif(TARGET ${dep}-shared)
                        set(actual_dep ${dep}-module)
                    endif()
                endif()
                target_link_libraries(${PARAM_TARGET} PRIVATE ${actual_dep})
            endforeach()
        endif()
    endif()
    _scm_status()
endfunction()
